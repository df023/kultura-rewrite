import { createContext, useContext } from "react";
import { types } from "mobx-state-tree";

import { UIStore, create as createUIStore } from "./UIStore";
import { ArtistsStore, create as createArtistsStore } from "./ArtistsStore";
import { ArtworksStore, create as createArtworksStore } from "./ArtworksStore";
import { PagesStore, create as createPagesStore } from "./PageStore";

export const StoreContext = createContext(null);
StoreContext.displayName = "StoreContext";

const RootStore = types.model("RootStore", {
  ui: UIStore,
  artists: ArtistsStore,
  artworks: ArtworksStore,
  pages: PagesStore,
});

export function createRootStore() {
  return RootStore.create({
    ui: createUIStore(),
    artists: createArtistsStore(),
    artworks: createArtworksStore(),
    pages: createPagesStore(),
  });
}

export function useStore() {
  return useContext(StoreContext);
}
