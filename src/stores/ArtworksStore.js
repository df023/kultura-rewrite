import { values } from "mobx";
import { types, flow, getRoot, detach } from "mobx-state-tree";
import { getArtworkById, getArtworkStats, getRandomArtworks, getCollectionById, getArtworkComments } from "api";
import { Artist } from "./ArtistsStore";

const ArtworkStats = types.model("ArtworkStats", {
  amountOfBuyers: types.maybeNull(types.number),
  amountOfComments: types.maybeNull(types.number),
  amountOfPurchases: types.maybeNull(types.number),
  amountOfWishlists: types.maybeNull(types.number),
  amountOfZooms: types.maybeNull(types.number),
});

const ArtworkComment = types.model("ArtworkComments", {
  id: types.identifierNumber,
  author: types.string,
  body: types.string,
  addedAt: types.string,
});

const ArtworkCollection = types.model("ArtworkCollection", {
  id: types.identifierNumber,
  name: types.string,
  url: "",
  description: "",
});

export const Artwork = types
  .model("Artwork", {
    id: types.identifierNumber,
    title: "",
    artistId: types.maybeNull(types.number),
    collectionId: types.maybeNull(types.number),
    collection: types.maybeNull(ArtworkCollection),
    date: "",
    medium: "",
    dimension: "",
    description: "",
    uploadedAt: types.string,
    artist: types.safeReference(Artist),
    stats: types.maybeNull(ArtworkStats),
    comments: types.maybeNull(types.array(ArtworkComment)),
    filename: types.string,
    width: types.maybeNull(types.number),
    height: types.maybeNull(types.number),
    isDetached: false,
  })
  .actions(self => ({
    beforeDetach() {
      self.isDetached = true;
    },
    loadArtist: flow(function* loadArtist() {
      if (self.artist || !self.artistId) return;

      const { artists } = getRoot(self);
      yield artists.loadArtist(self.artistId);

      // Todo(ars): find a better way to cancel/delete not (yet) loaded artist
      if (self.isDetached) {
        artists.delete(self.artistId);
      } else {
        self.artist = self.artistId;
      }
    }),
    loadStats: flow(function* LoadStats() {
      if (self.stats) return;

      self.stats = yield getArtworkStats(self.id);
    }),
    loadCollection: flow(function* loadCollection() {
      if (self.collection || !self.collectionId) return;

      self.collection = yield getCollectionById(self.collectionId);
    }),
    loadComments: flow(function* loadComments() {
      if (self.comments) return;

      const { comments } = yield getArtworkComments(self.filename);
      self.comments = comments;
    }),
  }));

export const ArtworksStore = types
  .model("ArtworksStore", {
    list: types.map(Artwork),
  })
  .actions(self => ({
    loadRandom: flow(function* loadRandom(amount) {
      const resp = yield getRandomArtworks(amount);

      return resp.map(artworkData => {
        const artwork = Artwork.create(artworkData);
        const id = self.list.put(artwork);

        artwork.loadArtist();
        artwork.loadStats();

        return id;
      });
    }),
    loadArtwork: flow(function* loadArtwork(artworkId) {
      if (self.list.get(artworkId)) return self.list.get(artworkId);

      const artwork = Artwork.create(yield getArtworkById(artworkId));

      self.list.put(artwork);

      artwork.loadArtist();
      artwork.loadStats();

      return artwork;
    }),
    deleteArtworks(artworks) {
      const { artists } = getRoot(self);
      const artistsToRemove = Array.from(
        new Set(artworks.filter(artwork => artwork.artist).map(artwork => artwork.artist.id))
      );

      artworks.forEach(detach);

      const referencedArtists = Array.from(
        new Set(
          values(self.list)
            .filter(artwork => artwork.artist)
            .map(artwork => artwork.artist.id)
        )
      );
      const unreferencedArtistIds = artistsToRemove.filter(artId => !referencedArtists.includes(artId));

      artists.deleteArtists(unreferencedArtistIds);
    },
    delete(artwork) {
      detach(artwork);
    },
  }));

export function create() {
  return ArtworksStore.create();
}
