import { types } from "mobx-state-tree";

const AuthModal = types
  .model("AuthModal", {
    visible: false,
    bgGradient: "",
  })
  .actions(self => ({
    toggle() {
      self.visible = !self.visible;
    },
    setBgGradient(gradient) {
      self.bgGradient = gradient;
    },
  }));

export const UIStore = types.model("UIStore", {
  authModal: AuthModal,
});

export function create() {
  return UIStore.create({ authModal: { visible: false } });
}
