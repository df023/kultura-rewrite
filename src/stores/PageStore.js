import { types, flow, getRoot } from "mobx-state-tree";
import { Artwork } from "./ArtworksStore";

const RootPage = types
  .model("RootPage", {
    isLoading: false,
    artworks: types.array(types.safeReference(Artwork)),
  })
  .actions(self => ({
    loadArtworks: flow(function* loadArtworks() {
      const rootStore = getRoot(self);

      if (self.artworks) {
        rootStore.artworks.deleteArtworks(self.artworks);
      }

      self.isLoading = true;
      self.artworks = yield rootStore.artworks.loadRandom(10);
      self.isLoading = false;
    }),
  }));

const ArtworkPage = types
  .model("ArtworkPage", {
    artwork: types.safeReference(Artwork),
  })
  .actions(self => ({
    loadArtwork: flow(function* loadArtwork(artworkId) {
      const artwork = yield getRoot(self).artworks.loadArtwork(artworkId);
      self.artwork = artwork;
      artwork.loadCollection();
      artwork.loadComments();
    }),
    removeReference() {
      self.artwork = undefined;
    },
  }));

export const PagesStore = types.model("PagesStore", {
  rootPage: RootPage,
  artworkPage: ArtworkPage,
});

export function create() {
  return PagesStore.create({ rootPage: RootPage.create(), artworkPage: ArtworkPage.create() });
}
