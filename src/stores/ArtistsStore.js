import { types, flow } from "mobx-state-tree";
import { getArtistById } from "api";

export const Artist = types.model("Artist", {
  id: types.identifierNumber,
  name: types.string,
  biography: types.maybeNull(types.string),
  description: types.maybeNull(types.string),
  links: types.array(types.string),
});

export const ArtistsStore = types
  .model("ArtistsStore", {
    list: types.map(Artist),
  })
  .actions(self => ({
    delete(artistId) {
      self.list.delete(artistId);
    },
    deleteArtists(artists) {
      artists.forEach(artistId => self.list.delete(artistId));
    },
    loadArtist: flow(function* loadArtist(artistId) {
      self.list.put(yield getArtistById(artistId));
    }),
  }));

export function create() {
  return ArtistsStore.create();
}
