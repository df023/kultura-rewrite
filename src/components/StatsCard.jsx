import PropTypes from "prop-types";
import { Icon, Card } from "./basic";

export default function StatsCard({ title, stats }) {
  return (
    <Card>
      <h2 className="text-24 font-medium px-22 py-26">{title}</h2>
      <ul className="bg-green-100 py-16 text-18 font-light">
        {stats.map(({ name, value }) => (
          <li key={name} className="px-24 py-4 flex justify-between odd:bg-green-200">
            {name} <strong className="font-bold">{value}</strong>
          </li>
        ))}
      </ul>
      <button type="button" className="underline flex items-center text-14 font-medium ml-20 my-15">
        <Icon icon="location" className="w-26 h-26 mr-6" />
        Explore in OWW Art Awakens
      </button>
    </Card>
  );
}

StatsCard.propTypes = {
  title: PropTypes.string.isRequired,
  stats: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};
