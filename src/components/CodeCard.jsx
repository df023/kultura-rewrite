import PropTypes from "prop-types";
import cn from "classnames";

export default function CodeCard({ className, number, cost, ...props }) {
  return (
    <div className={cn("inline-block", className)} {...props}>
      <p className="text-148 2xl:font-bold text-purple-300 text-opacity-50 text-center mt-40">{number}</p>
      <p className="text-34 font-bold  text-purple-300 text-opacity-50 text-center mt-95 ">{cost}</p>
    </div>
  );
}

CodeCard.propTypes = {
  className: PropTypes.string,
  number: PropTypes.string,
  cost: PropTypes.string,
};

CodeCard.defaultProps = {
  className: null,
  number: null,
  cost: null,
};
