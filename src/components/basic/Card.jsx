import PropTypes from "prop-types";
import cn from "classnames";

export default function Card({ className, ...props }) {
  return <div className={cn("bg-white-200 border border-white-400 rounded-5", className)} {...props} />;
}

Card.propTypes = {
  className: PropTypes.string,
};

Card.defaultProps = {
  className: null,
};
