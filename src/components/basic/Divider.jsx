import PropTypes from "prop-types";
import cn from "classnames";

export default function Divider({ className, height, color }) {
  return <hr className={cn("w-full border-none", height, color, className)} />;
}

Divider.propTypes = {
  className: PropTypes.string,
  height: PropTypes.string,
  color: PropTypes.string,
};

Divider.defaultProps = {
  className: null,
  height: "h-2",
  color: "bg-gray-500",
};
