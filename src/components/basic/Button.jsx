/* eslint-disable jsx-a11y/anchor-has-content,react/prop-types */
import { forwardRef } from "react";
import cn from "classnames";
import Link from "next/link";

function Button({ href, className, ...props }, ref) {
  const classnames = cn("inline-flex justify-center items-center", className);

  if (href) {
    return (
      <Link href={href}>
        <a ref={ref} className={classnames} {...props} />
      </Link>
    );
  }

  return <button ref={ref} className={classnames} type="button" {...props} />;
}

export default forwardRef(Button);
