import PropTypes from "prop-types";
import cn from "classnames";

export default function Image({ width, height, styles, alt, ...props }) {
  const aspectRatio = (height / width) * 100;

  return (
    <div
      className={cn("inline-block relative w-full bg-white-300", styles.container)}
      style={{ paddingTop: `${aspectRatio}%` }}
    >
      <img
        loading="lazy"
        className={cn("w-full h-full absolute inset-0", styles.image)}
        alt={alt}
        onError={e => {
          // eslint-disable-next-line no-param-reassign
          e.target.src = "/img/no-image.svg";
        }}
        {...props}
      />
    </div>
  );
}

Image.propTypes = {
  alt: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  styles: PropTypes.shape({
    container: PropTypes.string,
    image: PropTypes.string,
  }),
};

Image.defaultProps = {
  styles: { container: "", image: "" },
};
