export default function Input(props) {
  return (
    <input
      className="p-10 rounded-5 placeholder-gray-200 text-14 w-full bg-purple-200 border border-purple-100 focus:border-white-100"
      {...props}
    />
  );
}
