import PropTypes from "prop-types";

export default function Icon({ icon, ...props }) {
  return (
    <svg {...props}>
      <use xlinkHref={`/icons.svg#${icon}`} />
    </svg>
  );
}

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
};
