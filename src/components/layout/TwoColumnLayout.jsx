import PropTypes from "prop-types";
import cn from "classnames";

const Sidebar = ({ sticky, className, children }) => (
  <div
    className={cn(
      "flex-shrink-0 w-full max-w-496 mr-58",
      {
        "sticky top-20": sticky,
      },
      className
    )}
  >
    {children}
  </div>
);

const Children = PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.arrayOf(PropTypes.element)]);

Sidebar.propTypes = {
  className: PropTypes.string,
  sticky: PropTypes.bool,
  children: Children.isRequired,
};

Sidebar.defaultProps = {
  className: null,
  sticky: false,
};

const Main = ({ children }) => children;

Main.propTypes = {
  children: Children.isRequired,
};

export default function TwoColumnLayout({ className, children }) {
  const main = children.find(el => el.type === Main);
  const sidebar = children.find(el => el.type === Sidebar);

  return (
    <div className={cn("flex", { "items-start": sidebar.props.sticky }, className)}>
      {sidebar}
      {main}
    </div>
  );
}

TwoColumnLayout.propTypes = {
  className: PropTypes.string,
  children: Children.isRequired,
};

TwoColumnLayout.defaultProps = {
  className: null,
};

TwoColumnLayout.Sidebar = Sidebar;
TwoColumnLayout.Main = Main;
