import { useState, useRef } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "stores";

import Modal from "components/Modal";
import NavbarButton from "../NavbarButton";
import LoginForm from "./LoginForm";
import SignupForm from "./SignupForm";
import RecoveryForm from "./RecoveryForm";

const MODAL_STATE = {
  LOGIN: 0,
  SIGNUP: 1,
  RECOVER: 2,
};

function AuthModal() {
  const [modalState, setModalState] = useState(MODAL_STATE.LOGIN);
  const { ui } = useStore();
  const { authModal } = ui;

  const triggerEl = useRef();

  function handleModalClose() {
    authModal.toggle();
    setModalState(MODAL_STATE.LOGIN);
  }

  let form;

  switch (modalState) {
    case MODAL_STATE.SIGNUP:
      form = <SignupForm onLogin={() => setModalState(MODAL_STATE.LOGIN)} />;
      break;
    case MODAL_STATE.RECOVER:
      form = <RecoveryForm onLogin={() => setModalState(MODAL_STATE.LOGIN)} />;
      break;
    case MODAL_STATE.LOGIN:
    default:
      form = (
        <LoginForm
          onSignup={() => setModalState(MODAL_STATE.SIGNUP)}
          onRecovery={() => setModalState(MODAL_STATE.RECOVER)}
        />
      );
  }

  return (
    <>
      <NavbarButton ref={triggerEl} icon={authModal.visible ? "logout" : "login"} onClick={authModal.toggle} />
      <Modal
        className="w-full max-w-324 flex flex-col rounded-5 text-14 items-start text-white-100 bg-purple-300 bg-opacity-90"
        close={handleModalClose}
        isVisible={authModal.visible}
        triggerEl={triggerEl.current}
      >
        {form}
      </Modal>
    </>
  );
}

export default observer(AuthModal);
