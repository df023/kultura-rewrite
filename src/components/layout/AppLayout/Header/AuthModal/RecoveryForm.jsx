import { useState } from "react";
import PropTypes from "prop-types";
import Input from "components/basic/Input";
import Button from "./AuthModalButton";

export default function RecoveryForm({ onLogin }) {
  const [form, setForm] = useState({ email: "" });

  function handleSubmit(e) {
    e.preventDefault();

    setForm({ email: "" });
  }

  function handleChange(e) {
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  const canSubmit = true;

  return (
    <>
      <h2 className="text-24 mx-24 my-26">Account recovery</h2>
      <form
        className="w-full flex flex-col bg-purple-400 bg-opacity-80 rounded-5 p-12 space-y-10"
        onSubmit={handleSubmit}
      >
        <Input name="email" placeholder="Email" value={form.email} onChange={handleChange} />
        <p className="text-12 leading-14">
          You will shortly receive a password reset link by email, if you don&apos;t see it check your Spam folder too.
        </p>
        <Button type="submit" disabled={!canSubmit}>
          Send reset password email
        </Button>
      </form>
      <button type="button" className="mx-12 mt-18 mb-14 underline" onClick={onLogin}>
        Return to login
      </button>
    </>
  );
}

RecoveryForm.propTypes = {
  onLogin: PropTypes.func.isRequired,
};
