import { useState } from "react";
import PropTypes from "prop-types";
import Input from "components/basic/Input";
import Button from "./AuthModalButton";

export default function LoginForm({ onSignup, onRecovery }) {
  const [form, setForm] = useState({ username: "", password: "" });

  function handleSubmit(e) {
    e.preventDefault();

    setForm({ username: "", password: "" });
  }

  function handleChange(e) {
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  const canSubmit = form.username.length && form.password.length;

  return (
    <>
      <h2 className="text-24 mx-24 my-26">Login to Kultura</h2>
      <form
        className="w-full flex flex-col bg-purple-400 bg-opacity-80 rounded-5 p-12 space-y-10"
        onSubmit={handleSubmit}
      >
        <Input name="username" placeholder="Username" value={form.username} onChange={handleChange} />
        <Input name="password" type="password" placeholder="Password" value={form.password} onChange={handleChange} />
        <div className="flex justify-between">
          <Button type="submit" disabled={!canSubmit} className="max-w-105">
            Login
          </Button>
          <Button onClick={onSignup} className="max-w-105">
            Signup
          </Button>
        </div>
      </form>
      <button type="button" className="mx-12 mt-18 mb-14 underline" onClick={onRecovery}>
        Forgotten your password?
      </button>
    </>
  );
}

LoginForm.propTypes = {
  onSignup: PropTypes.func.isRequired,
  onRecovery: PropTypes.func.isRequired,
};
