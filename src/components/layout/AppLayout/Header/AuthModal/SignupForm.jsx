import { useState } from "react";
import PropTypes from "prop-types";
import Input from "components/basic/Input";
import Button from "./AuthModalButton";

function getInitialFormState() {
  return {
    email: "",
    confirmEmail: "",
    username: "",
    password: "",
    confirmPassword: "",
  };
}

export default function SignupForm({ onLogin }) {
  const [form, setForm] = useState(getInitialFormState());

  function handleSubmit(e) {
    e.preventDefault();

    setForm(getInitialFormState());
  }

  function handleChange(e) {
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  const canSubmit = true;

  return (
    <>
      <h2 className="text-24 mx-24 my-26">Create an account</h2>
      <form
        className="w-full flex flex-col bg-purple-400 bg-opacity-80 rounded-5 p-12 space-y-10"
        onSubmit={handleSubmit}
      >
        <Input name="email" placeholder="Email" value={form.email} onChange={handleChange} />
        <Input name="confirmEmail" placeholder="Confirm email" value={form.confirmEmail} onChange={handleChange} />
        <Input name="username" placeholder="Username" value={form.username} onChange={handleChange} />
        <Input type="password" name="password" placeholder="Password" value={form.password} onChange={handleChange} />
        <Input
          type="password"
          name="confirmPassword"
          placeholder="Confirm password"
          value={form.confirmPassword}
          onChange={handleChange}
        />
        <p className="text-12 leading-14">
          By creating an account you agree to our terms and conditions, code of conduct and privacy policy. We may send
          you the occasional email too. We will never sell your information.
        </p>

        <Button type="submit" disabled={!canSubmit}>
          Signup
        </Button>
      </form>
      <button type="button" onClick={onLogin} className="mx-12 mt-18 mb-14 underline">
        Already have an account?
      </button>
    </>
  );
}

SignupForm.propTypes = {
  onLogin: PropTypes.func.isRequired,
};
