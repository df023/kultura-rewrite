import PropTypes from "prop-types";
import cn from "classnames";
import BaseButton from "components/basic/Button";

export default function AuthModalButton({ className, disabled, ...props }) {
  return (
    <BaseButton
      className={cn(
        "font-medium w-full text-purple-300 p-10 rounded-5 active:bg-green-300",
        {
          "bg-green-400": !disabled,
          "bg-gray-200": disabled,
        },
        className
      )}
      disabled={disabled}
      {...props}
    />
  );
}

AuthModalButton.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
};

AuthModalButton.defaultProps = {
  className: null,
  disabled: false,
};
