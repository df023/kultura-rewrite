import SearchInput from "components/SearchInput";
import { URL } from "const";
import { useStore } from "stores";
import { observer } from "mobx-react-lite";
import { useRouter } from "next/router";
import NavbarButton from "./NavbarButton";
import AuthModal from "./AuthModal";
import PageTitle from "./PageTitle";

function Header() {
  const store = useStore();
  const router = useRouter();

  async function toRootPage() {
    await store.pages.rootPage.loadArtworks();
    await router.push(URL.ROOT);
  }

  return (
    <>
      <header className="bg-purple-300 text-white-100">
        <div className="container flex justify-between items-center px-15 py-9">
          <div className="flex-grow flex items-center">
            <button
              type="button"
              className="flex flex-shrink-0 mr-40"
              onClick={toRootPage}
              disabled={store.pages.rootPage.isLoading}
            >
              <img src="/img/kultura-logo.svg" alt="Kultura logo" />
            </button>
            <SearchInput className="mr-10" placeholder="Search by artist, collection, style, tag, etc." />
          </div>
          <nav className="flex space-x-24">
            <NavbarButton href={URL.GET_ART_CODES} icon="ticket">
              Get Art Codes
            </NavbarButton>
            <NavbarButton href={URL.UPLOAD_ARTWORK} icon="upload">
              Upload Artwork
            </NavbarButton>
            <NavbarButton href={URL.PROFILE_BOARDS} icon="pin">
              Boards
            </NavbarButton>
            <NavbarButton href={URL.PROFILE} icon="user">
              saint_patric_the_second
            </NavbarButton>
            <AuthModal />
          </nav>
        </div>
      </header>
      <PageTitle />
    </>
  );
}

export default observer(Header);
