/* eslint-disable react/prop-types */
import { forwardRef } from "react";
import cn from "classnames";

import { Icon, Button as BaseButton } from "components/basic";
import { useRouter } from "next/router";

function NavbarButton({ icon, href, children, ...props }, ref) {
  const router = useRouter();
  const isActive = router.pathname === href;

  return (
    <BaseButton
      ref={ref}
      href={href}
      className={cn("text-14 font-light", {
        "text-orange-100": isActive,
        "hover:text-orange-100": !isActive,
      })}
      {...props}
    >
      <Icon icon={icon} className="w-24 h-24 mr-14" />
      {children}
    </BaseButton>
  );
}

export default forwardRef(NavbarButton);
