import { useRouter } from "next/router";
import { useStore } from "stores";
import { observer } from "mobx-react-lite";
import { URL } from "const";
import styles from "./PageTitle.module.css";

const pageHeaders = {
  [URL.ROOT]: {
    title: () => "Welcome to the art portal for the PC Art and building MMO game OWW Art Awakens",
    gradient: "bg-gradient-1",
  },
  [URL.GET_ART_CODES]: {
    title: () => "Art Upload Code Packs",
    gradient: "bg-gradient-4",
  },
  [URL.UPLOAD_ARTWORK]: {
    title: () => "Upload Artwork",
    gradient: "bg-gradient-4",
  },
  [URL.PROFILE_BOARDS]: {
    title: () => "My Boards",
    gradient: "bg-gradient-2",
  },
  [URL.PROFILE]: {
    title: () => "Profile saint_patric_the_second",
    gradient: "bg-gradient-4",
  },
  [URL.ARTIST]: {
    title: () => "Édouard Manet",
    gradient: "bg-gradient-1",
  },
  [URL.ARTWORK]: {
    title: ({ pages }) => pages.artworkPage.artwork?.artist?.name,
    gradient: "bg-gradient-1",
  },
  [URL.COLLECTION]: {
    title: () => "Metropolitan Museum of Art",
    gradient: "text-white-100 bg-gradient-6",
    modalGradient: "bg-gradient-5",
  },
};

function PageTitle() {
  const router = useRouter();
  const store = useStore();
  const { authModal } = store.ui;
  const pageHeader = pageHeaders[router.pathname];

  if (!pageHeader) {
    authModal.setBgGradient("");
    return null;
  }

  authModal.setBgGradient(pageHeader.modalGradient || pageHeader.gradient);

  return (
    <div className={`${styles.pageTitle} ${pageHeader.gradient} text-24 font-light min-h-48`}>
      <div className="container px-58 py-11">{pageHeader.title(store)}</div>
    </div>
  );
}

export default observer(PageTitle);
