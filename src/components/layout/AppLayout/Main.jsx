import { useStore } from "stores";
import { observer } from "mobx-react-lite";
import PropTypes from "prop-types";
import cn from "classnames";
import styles from "./index.module.css";

function Main({ children }) {
  const { ui } = useStore();

  return (
    <main className={`${styles.main} container px-58 py-20 relative flex-grow flex flex-col`}>
      {children}
      {ui.authModal.visible && <div className={cn("absolute inset-0 opacity-50", ui.authModal.bgGradient)} />}
    </main>
  );
}

Main.propTypes = {
  children: PropTypes.element.isRequired,
};

export default observer(Main);
