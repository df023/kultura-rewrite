import PropTypes from "prop-types";
import Header from "./Header";
import Main from "./Main";
import Footer from "./Footer";

export default function Layout({ children }) {
  return (
    <div className="flex flex-col">
      <Header />
      <Main>{children}</Main>
      <Footer />
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.element.isRequired,
};
