export default function Footer() {
  return (
    <footer className="text-11 text-purple-300 bg-gray-400">
      <div className="container flex justify-between items-center px-58 py-4">
        &copy; Stikipixels Limited 2021. Unless otherwise indicated, all rights in each work protected by copyright are
        vested in the individual artist who created such work and are used by StikiPixels Limited/OWW with the
        permission of said artists.
        <a href="https://www.stikipixels.com/" target="_blank" rel="noreferrer" className="flex flex-shrink-0">
          <img src="/img/stikipixels-logo.png" alt="stikipixels logo" />
        </a>
      </div>
    </footer>
  );
}
