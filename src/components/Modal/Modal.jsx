import PropTypes from "prop-types";
import cn from "classnames";
import { useEffect, useRef } from "react";
import { createPortal } from "react-dom";

import styles from "./Modal.module.css";

export default function Modal({ close, className, isVisible, triggerEl, ...props }) {
  const modalRef = useRef();

  useEffect(() => {
    function handleModalClose(e) {
      const path = e.composedPath();

      if (path.includes(modalRef.current) || path.includes(triggerEl)) {
        return;
      }

      close();
    }

    if (isVisible) {
      document.addEventListener("mousedown", handleModalClose);
      return () => document.removeEventListener("mousedown", handleModalClose);
    }

    return null;
  }, [isVisible]);

  if (!isVisible) return null;

  return createPortal(
    <div ref={modalRef} {...props} className={cn(`${styles.modal} fixed left-0 top-0`, className)} />,
    document.body
  );
}

Modal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  className: PropTypes.string,
  close: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  className: null,
};
