import PropTypes from "prop-types";
import cn from "classnames";

import { Icon, Button as BaseButton } from "./basic";

export default function Button({ icon, iconStyles, color, className, children, ...props }) {
  const baseStyles = ["rounded-5 px-15 py-10"];

  switch (color) {
    case "green":
      baseStyles.push("text-purple-300 bg-green-400");
      break;
    case "purple":
    default:
      baseStyles.push("text-white-100 bg-purple-300 ");
  }

  return (
    <BaseButton className={cn(baseStyles, className)} {...props}>
      {icon && <Icon icon={icon} className={iconStyles} />}
      {children}
    </BaseButton>
  );
}

Button.propTypes = {
  color: PropTypes.oneOf(["purple", "green"]),
  icon: PropTypes.string,
  iconStyles: PropTypes.string,
  children: PropTypes.string,
  className: PropTypes.string,
};

Button.defaultProps = {
  color: "purple",
  icon: null,
  iconStyles: "w-20 h-20 mr-20",
  children: null,
  className: null,
};
