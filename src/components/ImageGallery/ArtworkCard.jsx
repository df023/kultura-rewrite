import PropTypes from "prop-types";
import Link from "next/link";
import { observer } from "mobx-react-lite";
import { getArtistUrl, getArtworkUrl, IMAGE_TYPE } from "const";
import ImageCard from "./ImageCard";

function ArtworkCard({ artwork, artistLink, ...props }) {
  const { id, title, artist } = artwork;
  const stats = artwork.stats
    ? [artwork.stats.amountOfBuyers, artwork.stats.amountOfComments, artwork.stats.amountOfWishlists]
    : [];

  return (
    <ImageCard
      image={{ ...artwork, stats }}
      url={getArtworkUrl(id, title)}
      type={IMAGE_TYPE.ARTWORK}
      renderTitle={() =>
        artistLink && artist ? (
          <p className="flex flex-col items-start break-words text-18 leading-22 font-light">
            <Link href={getArtistUrl(artist.id, artist.name)}>
              <a className="max-w-full text-14 font-bold break-words hover:text-purple-100">{artist.name}</a>
            </Link>
            <span className="max-w-full">{title}</span>
          </p>
        ) : (
          title
        )
      }
      {...props}
    />
  );
}

ArtworkCard.propTypes = {
  artwork: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    artist: PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }),
    stats: PropTypes.shape({
      amountOfBuyers: PropTypes.number,
      amountOfComments: PropTypes.number,
      amountOfWishlists: PropTypes.number,
    }),
  }).isRequired,
  artistLink: PropTypes.bool,
};

ArtworkCard.defaultProps = {
  artistLink: true,
};

export default observer(ArtworkCard);
