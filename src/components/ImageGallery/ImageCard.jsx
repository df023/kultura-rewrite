import Link from "next/link";
import cn from "classnames";
import PropTypes from "prop-types";
import { IMAGE_TYPE } from "const";
import { getArtworkFilePath } from "api";
import { Image, Divider } from "components/basic";
import styles from "./ImageCard.module.css";

const STAT_HEADERS = {
  [IMAGE_TYPE.ARTIST]: ["Artworks", "Comments", "Acquired"],
  [IMAGE_TYPE.ARTWORK]: ["Acquired", "Comments", "Wishlists"],
  // TODO(ars): add stats for collections and boards
  [IMAGE_TYPE.COLLECTION]: null,
  [IMAGE_TYPE.BOARD]: null,
};

export default function ImageCard({ image, url, type, renderTitle, className }) {
  const { filename, stats, title, width, height } = image;

  const statHeaders = STAT_HEADERS[type];

  return (
    <div className={cn("inline-block w-full max-w-290", className)}>
      <Link href={url}>
        <a className="inline-flex w-full">
          <Image
            width={width}
            height={height}
            alt={title}
            styles={{ container: "overflow-hidden", image: `${styles.image} opacity-0` }}
            src={getArtworkFilePath(filename)}
            onLoad={e => e.target.classList.remove("opacity-0")}
          />
        </a>
      </Link>
      <div className="mt-8">{renderTitle(title)}</div>
      {statHeaders && !!stats.length && (
        <>
          <Divider color="bg-white-300" className="mb-8 mt-10" />
          <div className="flex space-x-20 text-14 font-light">
            {statHeaders.map((header, index) => (
              <p key={header} className="flex flex-col">
                {header}
                <strong className="mt-6 font-bold">{stats[index]}</strong>
              </p>
            ))}
          </div>
        </>
      )}
    </div>
  );
}

export const ImageType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  filename: PropTypes.string.isRequired,
  // Note(ars): width and height could be missing
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  stats: PropTypes.arrayOf(PropTypes.number).isRequired,
});

ImageCard.propTypes = {
  renderTitle: PropTypes.func,
  url: PropTypes.string.isRequired,
  type: PropTypes.oneOf(Object.values(IMAGE_TYPE)).isRequired,
  image: ImageType.isRequired,
  className: PropTypes.string,
};

ImageCard.defaultProps = {
  renderTitle: title => title,
  className: null,
};
