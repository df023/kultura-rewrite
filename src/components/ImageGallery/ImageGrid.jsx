import PropTypes from "prop-types";

export default function ImageGrid({ children, size }) {
  return (
    <div style={{ columns: size }}>
      {children.map((child, index) => (
        <div key={index} className="relative inline-flex w-full max-w-290">
          {child}
          <div className="flex justify-center items-center absolute bg-purple-400 bg-opacity-50 text-18 font-medium w-40 h-40 text-white-100">
            {index + 1}
          </div>
        </div>
      ))}
    </div>
  );
}

ImageGrid.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  size: PropTypes.number,
};

ImageGrid.defaultProps = {
  size: 4,
};
