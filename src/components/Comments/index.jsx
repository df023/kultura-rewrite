import PropTypes from "prop-types";
import cn from "classnames";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { pluralize } from "utils";
import { Button, Divider, Icon } from "components/basic";

dayjs.extend(relativeTime);

export default function Comments({ comments, className }) {
  return (
    <div className={cn("text-14 font-light", className)}>
      <div className="flex items-center mb-14">
        <p className="flex-shrink-0 flex items-center font-medium">
          <Icon icon="chat-bubble" className="w-20 h-20 mr-10" />
          {pluralize(comments.length, "comment")}
        </p>
        <Divider className="mx-6" />
        <Button className="flex-shrink-0 underline font-medium">
          <Icon icon="plus" className="w-18 h-18 mr-6" />
          Add your comment
        </Button>
      </div>
      <ul className="space-y-20">
        {comments.map(({ author, addedAt, body }, index) => (
          <li key={index}>
            <p className="text-gray-100 mb-14">
              <em>{author}</em> {dayjs(addedAt).fromNow()}
            </p>
            <p className="leading-18">{body}</p>
          </li>
        ))}
      </ul>
    </div>
  );
}

const CommentType = PropTypes.shape({
  addedAt: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
});

Comments.propTypes = {
  className: PropTypes.string,
  comments: PropTypes.arrayOf(CommentType),
};

Comments.defaultProps = {
  className: null,
  comments: [],
};
