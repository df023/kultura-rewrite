import cn from "classnames";
import PropTypes from "prop-types";

export default function SearchInput({ className, ...props }) {
  return (
    <input
      className={cn(
        "p-10 rounded-5 placeholder-gray-200 text-14 leading-18 w-full max-w-680 bg-purple-200 border border-purple-100 focus:border-orange-100",
        className
      )}
      {...props}
    />
  );
}

SearchInput.propTypes = {
  className: PropTypes.string,
};

SearchInput.defaultProps = {
  className: "",
};
