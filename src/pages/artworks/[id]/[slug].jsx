import { useEffect } from "react";
import { useRouter } from "next/router";
import { useStore } from "stores";
import { observer } from "mobx-react-lite";
import Head from "next/head";
import Link from "next/link";
import { Card, Divider, Icon, Image } from "components/basic";
import Button from "components/Button";
import Comments from "components/Comments";
import { IMAGE_SIZE_PREFIX, getArtistUrl, getCollectionUrl } from "const";
import { formatDate } from "utils";
import { getArtworkFilePath } from "api";

function ArtworkPage() {
  const router = useRouter();
  const store = useStore();

  useEffect(() => {
    if (router.isReady) {
      const artworkId = Number(router.query.id);
      store.pages.artworkPage.loadArtwork(artworkId);
    }

    return () => {
      store.pages.artworkPage.removeReference();
    };
  }, [router.isReady]);

  const { artwork } = store.pages.artworkPage;

  if (!artwork) return null;

  const {
    title,
    artist,
    stats,
    collection,
    comments,
    date,
    filename,
    medium,
    dimension,
    description,
    uploadedAt,
    width,
    height,
  } = artwork;

  return (
    <>
      <Head>
        <title>Kultura - {title}</title>
      </Head>
      <div className="flex items-start text-black-100 space-x-58">
        <div className="flex-shrink-0 flex flex-col items-start w-full max-w-570">
          <Image
            width={width}
            height={height}
            styles={{ container: "bg-white-300", image: "transition-opacity duration-150 ease-in opacity-0" }}
            onLoad={e => e.target.classList.remove("opacity-0")}
            src={getArtworkFilePath(filename, IMAGE_SIZE_PREFIX.MEDIUM)}
            alt={title}
          />
          <p className="flex items-center text-14 font-light text-gray-100 mt-10 ml-10">
            <Icon icon="expand" className="w-18 h-18 mr-10 text-black-100" />
            Click on the image to enlarge
          </p>
        </div>
        <div className="flex-grow text-14 leading-18 font-light">
          {artist && (
            <Link href={getArtistUrl(artist.id, artist.name)}>
              <a className="inline-block underline font-medium mb-10 leading-none">{artist.name}</a>
            </Link>
          )}

          <h2 className="text-34 leading-44 font-medium mb-10">{title}</h2>
          <p className="text-24 mb-40">{date}</p>
          <p>{medium}</p>
          <p>{dimension}</p>
          {description && <p className="mt-40">{description}</p>}

          <p className="mt-20 text-gray-100 ">Uploaded on {formatDate(uploadedAt)}</p>
          {collection && (
            <div className="mt-20">
              <Divider className="mb-16" />
              <p className="mb-10">Collection:</p>
              <Button href={getCollectionUrl(collection.id, collection.name)} className="font-normal">
                {collection.name}
              </Button>
            </div>
          )}

          {/* Boards management  */}
          {/*<div className="mt-20">*/}
          {/*  <Divider className="mb-16" />*/}
          {/*  <p className="mb-10">*/}
          {/*    Artwork added to <em>19th-century paintings</em>*/}
          {/*  </p>*/}
          {/*  <Button icon="pin" iconStyles="w-20 h-20 mr-14" color="green" className="font-normal">*/}
          {/*    Manage Boards*/}
          {/*  </Button>*/}
          {/*</div>*/}
        </div>
        <Card className="static top-20 flex-shrink-0 w-full max-w-494 px-24 py-18">
          <h2 className="text-24 font-medium mb-26">Recent Activity in OWW Art Awakens</h2>
          {stats && (
            <div className="flex space-x-24 bg-green-100 px-24 py-22 -mx-24 text-18 leading-22 font-light uppercase">
              <p className="flex flex-col">
                acquired <strong className="font-bold">{stats.amountOfBuyers}</strong> times
              </p>
              <p className="flex flex-col">
                received <strong className="font-bold">{stats.amountOfComments}</strong> comments
              </p>
              <p className="flex flex-col">
                saved on <strong className="font-bold">{stats.amountOfWishlists}</strong> wishlists
              </p>
              <p className="flex flex-col">
                zoomed <strong className="font-bold">{stats.amountOfZooms}</strong> times
              </p>
            </div>
          )}
          <a href="https://www.oww.io/" target="_blank" rel="noreferrer" className="inline-flex mb-10">
            <img src="/img/oww-banner.png" alt="OWW Banner" />
          </a>
          {comments && <Comments comments={comments} />}
        </Card>
      </div>
    </>
  );
}

export default observer(ArtworkPage);
