import Head from "next/head";
import PropTypes from "prop-types";
import "assets/styles/index.css";
import { AppLayout } from "components/layout";
import { StoreContext, createRootStore } from "stores";

const rootStore = createRootStore();

export default function App({ Component, pageProps }) {
  return (
    <StoreContext.Provider value={rootStore}>
      <Head>
        <title>Kultura</title>
      </Head>
      <AppLayout>
        <Component {...pageProps} />
      </AppLayout>
    </StoreContext.Provider>
  );
}

App.propTypes = {
  Component: PropTypes.oneOfType([PropTypes.elementType, PropTypes.func]).isRequired,
  pageProps: PropTypes.shape().isRequired,
};
