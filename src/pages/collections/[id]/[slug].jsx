import Head from "next/head";
import { ArtworkCard, ImageGrid } from "components/ImageGallery";
import Divider from "components/basic/Divider";
import Button from "components/Button";
import images from "testImages/collection.json";
import { TwoColumnLayout } from "components/layout";
import StatsCard from "components/StatsCard";

export default function CollectionPage() {
  return (
    <>
      <Head>
        <title>Kultura - Metropolitan Museum of Art</title>
      </Head>
      <TwoColumnLayout>
        <TwoColumnLayout.Sidebar sticky>
          <div className="text-14 text-black-100 font-light">
            <p className="text-gray-100 mb-6">About the Collection</p>
            <h2 className="text-34 font-medium leading-44 mb-14">Metropolitan Museum of Art</h2>
          </div>
          <Divider className="my-26" />
          <p className="text-14 font-light text-gray-100 mb-16">Visit Collection Website</p>
          <Button href="https://google.com" target="_blank" rel="noreferrer" className="text-14 font-normal h-40 mb-26">
            Metropolitan Museum of Art
          </Button>
          <StatsCard
            title="Collection stats in OWW Art Awakens"
            stats={[
              { name: "Artworks in game", value: "42" },
              { name: "Artworks purchased", value: "9,925" },
              { name: "Unique buyers", value: "4,459" },
              { name: "Wishlisted", value: "467" },
              { name: "Comments", value: "138" },
            ]}
          />
        </TwoColumnLayout.Sidebar>
        <TwoColumnLayout.Main>
          <ImageGrid size={4}>
            {images.map(image => (
              <ArtworkCard key={image.id} artwork={image} className="mb-20" />
            ))}
          </ImageGrid>
        </TwoColumnLayout.Main>
      </TwoColumnLayout>
    </>
  );
}
