import Button from "components/Button";
import Divider from "components/basic/Divider";
import CodeCard from "components/CodeCard";
import { TwoColumnLayout } from "components/layout";

// const CodeImagePlaceholders = new Array(5).fill(0);

export default function GetArtCodesPage() {
  return (
    <TwoColumnLayout className="flex-grow text-black-100">
      <TwoColumnLayout.Sidebar>
        <p className="text-gray-100 text-14 font-light mb-20">You can upload your Art!</p>
        <h2 className="text-34 font-medium mb-24">Purchase Art Codes</h2>
        <p className="text-14 font-light mb-20">
          Art Codes allow artists to upload artworks into Occupy White Walls (OWW) and Kultura. One Code = one artworks.
          <span className="text-black-100 font-bold"> Not an artist? </span>
          You can still buy codes and gift them to artists you like, they can register an account on Kultura (No game
          install required) and use these codes to upload art
        </p>
        <p className="text-14 font-light mb-20">
          Each artwork in OWW enters DAISY, our AI. DAISY will promote your art to people who will most likely resonate
          with your art. DAISY allows you to discover new audiences across the world that you never have otherwise. Each
          artist in OWW has a profile, stats and the ability to add external files directly within the games interface,
          players to click, follow, and reach out to you and buy art.
        </p>
        <p className="text-14 font-light mb-20">For more more detailed info, check out the artists FAQ</p>
        <Divider className="mb-15" />
        <div className="mb-26">
          <p className="text-14 font-light text-gray-100 mb-15">
            Your Email: <span className="text-black-100 font-bold">Username@mail.com</span>
          </p>
        </div>
        <div className="mb-26">
          <input type="checkbox" id="terms" name="terms" />
          <label htmlFor="terms" className="text-14 font-light mb-20">
            I accept the terms and conditions
          </label>
        </div>
        <div className="flex">
          <Button icon="ticket">Purchase Now</Button>
        </div>
      </TwoColumnLayout.Sidebar>
      <TwoColumnLayout.Main>
        <div className="w-full flex">
          <CodeCard
            number={1}
            cost="9$USD"
            className="w-1/5 max-w-290 h-3/6 mb-20 bg-gradient-to-r from-orange-100  via-orange-100 to-pink-100 m-4"
          />
          <CodeCard
            number={3}
            cost="25$USD"
            className="w-1/5 max-w-290 h-3/6 mb-20 bg-gradient-to-r from-pink-100 to-purple-100 m-4"
          />
          <CodeCard number={6} cost="48$USD" className="w-1/5 max-w-290 h-3/6 mb-20 bg-blue-200 m-4" />
          <CodeCard
            number={10}
            cost="75$USD"
            className="w-1/5 max-w-290 h-3/6 mb-20 bg-gradient-to-r from-pink-100  to-pink-500 m-4"
          />
          <CodeCard
            number={14}
            cost="95$USD"
            className="w-1/5 max-w-290 h-3/6 mb-20 bg-gradient-to-r from-pink-100   via-blue-100 to-blue-100 m-4"
          />
        </div>
      </TwoColumnLayout.Main>
    </TwoColumnLayout>
  );
}
