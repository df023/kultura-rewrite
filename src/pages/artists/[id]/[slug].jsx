import Head from "next/head";
import Divider from "components/basic/Divider";
import { ImageGrid, ArtworkCard } from "components/ImageGallery";
import { TwoColumnLayout } from "components/layout";
import images from "testImages/artist.json";
import StatsCard from "components/StatsCard";

export default function ArtistPage() {
  return (
    <>
      <Head>
        <title>Kultura - Édouard Manet</title>
      </Head>
      <TwoColumnLayout>
        <TwoColumnLayout.Sidebar sticky>
          <div className="text-14 text-black-100 font-light">
            <p className="text-gray-100 mb-6">About the artist</p>
            <h2 className="text-34 font-medium leading-44 mb-14">Édouard Manet</h2>
            <p className="leading-18">
              Édouard Manet (23 January 1832 – 30 April 1883) was a French modernist painter. He was one of the first
              19th-century artists to paint modern life, and a pivotal figure in the transition from Realism to
              Impressionism. Born into an upper-class household with strong political connections, Manet rejected the
              future originally envisioned for him, and became engrossed in the world of painting. His early
              masterworks, The Luncheon on the Grass (Le déjeuner sur l&apos;herbe) and Olympia, both 1863, caused great
              controversy and served as rallying points for the young painters who would create Impressionism. Today,
              these are considered watershed paintings that mark the start of modern art. The last 20 years of
              Manet&apos;s life saw him form bonds with other great artists of the time, and develop his own style that
              would be heralded as innovative and serve as a major influence for future painters.
            </p>
          </div>
          <Divider className="my-26" />
          <StatsCard
            title="Artist stats in OWW Art Awakens"
            stats={[
              { name: "Artworks in game", value: "42" },
              { name: "Artworks purchased", value: "9,925" },
              { name: "Unique buyers", value: "4,459" },
              { name: "Wishlisted", value: "467" },
              { name: "Comments", value: "138" },
            ]}
          />
        </TwoColumnLayout.Sidebar>
        <TwoColumnLayout.Main>
          <ImageGrid size={4}>
            {images.map(artwork => (
              <ArtworkCard key={artwork.id} artwork={artwork} artistLink={false} className="mb-20" />
            ))}
          </ImageGrid>
        </TwoColumnLayout.Main>
      </TwoColumnLayout>
    </>
  );
}
