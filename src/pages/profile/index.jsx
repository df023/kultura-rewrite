import { Card, Icon, Divider } from "components/basic";
import Button from "components/Button";
import StatsCard from "components/StatsCard";
import { TwoColumnLayout } from "components/layout";

import styles from "./index.module.css";

const vaultImagePlaceholders = new Array(15).fill(0);

export default function ProfilePage() {
  return (
    <TwoColumnLayout className="flex-grow text-black-100">
      <TwoColumnLayout.Sidebar>
        <p className="text-gray-100 text-14 font-light mb-20">Logged in as</p>
        <h2 className="text-34 font-medium mb-24">saint_patric_the_second</h2>
        <p className="text-14 font-light mb-20">View your used and remaining codes for uploading artwork here:</p>
        <ul className="text-14 font-bold space-y-4 mb-24">
          <li>Remaining codes (52)</li>
          <li>Used codes (23)</li>
        </ul>
        <Divider className="mb-15" />
        <div className="mb-26">
          <p className="text-14 font-light text-gray-100 mb-15">Want to show your work?</p>
          <div className="flex">
            <Button icon="ticket" className="mr-10">
              Get Art Codes
            </Button>
            <Button icon="upload">Upload Artwork</Button>
          </div>
        </div>
        <StatsCard
          title="Your stats in OWW Art Awakens"
          stats={[
            { name: "Artworks uploaded", value: "23" },
            { name: "Owned Galleries", value: "5" },
            { name: "Gallery Name", value: "Untitled, Vormir + 3" },
            { name: "Players in Gallery now", value: "0" },
            { name: "Last Login", value: "1 day ago" },
          ]}
        />
      </TwoColumnLayout.Sidebar>
      <TwoColumnLayout.Main>
        <Card className="w-full flex flex-col">
          <div className="flex justify-between items-center px-20 py-15">
            <div className="flex-shrink-0 flex items-center">
              <h2 className="text-24 font-medium mr-16">My Vault</h2>
              <p className="text-14 font-light text-gray-100">You own 754 Artworks in OWW Art Awakens</p>
            </div>
            <div className="flex-grow flex items-center justify-end">
              <input
                className="p-10 rounded-5 placeholder-gray-100 bg-white-500 border border-gray-600 leading-18 w-full max-w-680 text-14 font-light mr-18"
                placeholder="Search by artist, collection, style, tag, etc."
              />
              <button type="button">
                <Icon icon="plus" className="w-18 h-18" />
              </button>
            </div>
          </div>
          <div className={`${styles.vaultGrid} flex-grow p-15 bg-green-100`}>
            {vaultImagePlaceholders.map((_, index) => (
              <div key={index} className="w-140 h-140 rounded-5 bg-green-300" />
            ))}
          </div>
          <div className="flex justify-between items-center mt-auto">
            <button type="button" className="underline flex items-center text-14 font-medium ml-20 my-15">
              <Icon icon="location" className="w-26 h-26 mr-6" />
              Explore in OWW Art Awakens
            </button>
            <div className="flex items-center">
              <p className="text-14 font-light text-gray-100 mr-18">
                You can also organise your Artworks on your boards
              </p>
              <Button icon="pin" iconStyles="w-18 h-18 mr-14" color="green" className="text-14 font-medium mr-15">
                Manage Boards
              </Button>
            </div>
          </div>
        </Card>
      </TwoColumnLayout.Main>
    </TwoColumnLayout>
  );
}
