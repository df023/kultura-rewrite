import Icon from "components/basic/Icon";

export default function BoardsPage() {
  return (
    <div className="w-full max-w-1235 mx-auto mt-38 flex justify-center flex-wrap gap-26">
      <div className="w-290 h-290 bg-white-600 rounded-10" />
      <div className="w-290 h-290 bg-white-600 rounded-10" />
      <div className="w-290 h-290 bg-white-600 rounded-10" />
      <div className="w-290 h-290 bg-white-600 rounded-10" />
      <div className="w-290 h-290 bg-white-600 rounded-10 flex justify-center items-center text-gray-800">
        <Icon icon="plus" className="w-95 h-95" />
      </div>
    </div>
  );
}
