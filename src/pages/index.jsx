import { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { ImageGrid, ArtworkCard } from "components/ImageGallery";
import { useStore } from "stores";

function RootPage() {
  const { pages } = useStore();

  useEffect(() => {
    if (!pages.rootPage.artworks.length) {
      pages.rootPage.loadArtworks();
    }
  }, []);

  return (
    <ImageGrid artworks={pages.rootPage.artworks} size={6}>
      {pages.rootPage.artworks.map(artwork => (
        <ArtworkCard key={artwork.id} artwork={artwork} className="mb-20" />
      ))}
    </ImageGrid>
  );
}

export default observer(RootPage);
