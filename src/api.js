import Cookies from "js-cookie";
import { IMAGE_SIZE_PREFIX } from "const";

const baseUrl = `${process.env.NEXT_PUBLIC_BACKEND_ADDRESS}/api/v1`;

const accessToken = Cookies.get("access_token");
const username = Cookies.get("username");

const headers = { "Content-Type": "application/json" };

if (accessToken) headers.Authorization = `Bearer ${accessToken}`;
if (username) headers["X-Auth-Username"] = username;

async function http(path, { body, ...options }) {
  const resp = await fetch(`${baseUrl}${path}`, {
    ...options,
    body: JSON.stringify(body),
    headers,
  });

  if (!resp.ok) {
    throw new Error("Error contacting the server");
  }

  return resp.json();
}

http.get = path => http(path, { method: "GET" });

http.post = (path, payload = null) =>
  http(path, {
    method: "POST",
    body: payload,
  });

export function getArtworkFilePath(filename, size = IMAGE_SIZE_PREFIX.SMALL) {
  return `${baseUrl}/files/artworks/${size}${filename}`;
}

export async function getArtistById(artistId) {
  return http.post("/artist/get", { artistId });
}

export async function getArtworkById(id) {
  return http.post("/artwork/get_by_id", { id });
}

export async function getArtworkStats(id) {
  return http.post("/artwork/stats", { id });
}

export async function getCollectionById(collectionId) {
  return http.post("/artwork/collection/get", { collectionId });
}

export async function getArtworkComments(artworkFilename) {
  return http.post("/comments/get_for_artwork", { artworkFilename });
}

export async function getRandomArtworks(amount) {
  const { artworks } = await http.post("/artwork/random", { amount });
  return artworks;
}
