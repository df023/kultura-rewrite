import slugify from "slugify";

export const URL = {
  ROOT: "/",
  GET_ART_CODES: "/get-art-codes",
  UPLOAD_ARTWORK: "/artworks/upload",
  PROFILE: "/profile",
  PROFILE_BOARDS: "/profile/boards",
  ARTIST: "/artists/[id]/[slug]",
  ARTWORK: "/artworks/[id]/[slug]",
  COLLECTION: "/collections/[id]/[slug]",
};

const getSlug = str => slugify(str, { lower: true, strict: true });

export const getArtistUrl = (id, name) => `/artists/${id}/${getSlug(name)}`;
export const getArtworkUrl = (id, title = "") => `/artworks/${id}/${getSlug(title)}`;
export const getCollectionUrl = (id, name) => `/collections/${id}/${getSlug(name)}`;

export const IMAGE_SIZE_PREFIX = {
  THUMBNAIL: "u1v2w0_",
  SMALL: "d1e2f0_",
  MEDIUM: "a1b2c0_",
  LARGE: "x1y2z0_",
};

export const IMAGE_TYPE = {
  ARTIST: "artist",
  ARTWORK: "artwork",
  COLLECTION: "collection",
  BOARD: "board",
};
