import dayjs from "dayjs";

export function formatDate(date, format = "M/D/YYYY") {
  return dayjs(date).format(format);
}

export function pluralize(num, str, suffix = "s") {
  return `${num} ${str}${num !== 1 ? suffix : ""}`;
}
